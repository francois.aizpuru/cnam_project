<?php if ($this->Session->isAdmin()): ?>
  <table>
    <tr>
      <th>Pseudo</th>
      <th>Mot de passe</th>
      <th>Admin</th>
      <th>Switch Admin</th>
    </tr>
    <?php foreach ($users as $key => $value): ?>
      <tr>
        <td><?php echo $value->pseudo ?></td>
        <td><a href=<?php echo '"'.BASE_URL.DS.'auth'.DS.'password'.DS.$value->id.'"' ?>>Changer mot de passe</a></td>
        <td><?php echo $value->admin==1?"Admin":"Utilisateur"; ?></td>
        <td><a href=<?php echo '"'.BASE_URL.DS.'auth'.DS.'switch'.DS.$value->id.'"' ?>>Switch Admin</a></td>
      </tr>
    <?php endforeach; ?>
  </table>
<?php endif; ?>
