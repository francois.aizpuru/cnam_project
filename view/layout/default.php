<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo isset($title_for_layout)?$title_for_layout:"Projet Recettes CNAM" ?></title>
    <?php echo '<link rel="stylesheet" href="'.BASE_URL.DS.'css'.DS.'style.css" type="text/css">'?>
    <?php echo '<script src="'.BASE_URL.DS.'js'.DS.'scripts.js"></script>'?>

    <link href="https://fonts.googleapis.com/css?family=Roboto|Spirax" rel="stylesheet">
  </head>
  <body>
    <header>
      <div class="left">
        <div class="logo">
          <h1>Recipes</h1>
        </div>
        <div class="links">
          <a href=<?php echo '"'.BASE_URL.DS.'recipes'.DS.'index'.'"' ?>>Recettes</a>
          <a href=<?php echo '"'.BASE_URL.DS.'users'.DS.'index'.'"' ?>>Utilisateurs</a>
        </div>
      </div>
      <div class="center"></div>
      <div class="right">
        <div class="auth_header">
          <?php if (isset($this->Session) && !$this->Session->isLogged()): ?>
            <a href=<?php echo '"'.BASE_URL.DS.'auth'.DS.'login'.'"' ?>>Connexion</a>
            <a href=<?php echo '"'.BASE_URL.DS.'auth'.DS.'inscription'.'"' ?>>Inscription</a>
          <?php else: ?>
            <?php if (isset($this->Session) && $this->Session->isAdmin()): ?>
              <a href=<?php echo '"'.BASE_URL.DS.'admin'.DS.'manage'.'"' ?>>Admin</a>
            <?php endif; ?>
            <a href=<?php echo '"'.BASE_URL.DS.'auth'.DS.'password'.'"' ?>>Compte</a>
            <a href=<?php echo '"'.BASE_URL.DS.'auth'.DS.'logout'.'"' ?>>Se déconnecter</a>
          <?php endif; ?>
        </div>
      </div>
    </header>
    <?php echo($content_to_inject) ?>
    <footer>
      <div class="copyright">
        <p>
          <?php echo 'Copyright '.date("Y").' François AIZPURU' ?>
        </p>
      </div>
    </footer>
  </body>
</html>
