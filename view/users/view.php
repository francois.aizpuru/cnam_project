<?php if ($this->Session->isLogged()): ?>

<h1>Liste des recettes:</h1>

<div class="list_recipes">
  <?php foreach ($recipes as $key => $value): ?>
    <div class="recipe">
      <h2>
        <?php echo $value->name ?>
      </h2>
      <?php echo $value->private?"Privée":"Public" ?>
      <p>
        <?php echo $value->description ?>
      </p>
      <a href=<?php echo '"'.BASE_URL.DS.'recipes'.DS.'view'.DS.$value->id.'"' ?>>Accéder à la recette</a>
    </div>
  <?php endforeach; ?>
</div>

<?php else: ?>
  Veuillez vous connecter
<?php endif; ?>
