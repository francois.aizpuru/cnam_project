<?php if ($this->Session->isLogged()): ?>

<?php $title_for_layout= $recipe->name ?>
<h1><?php echo $recipe->name ?></h1>
<?php if ($this->Session->isAdmin() || $recipe->users_id==$this->Session->userId()): ?>
  <a class="action_button" href=<?php echo '"'.BASE_URL.DS.'recipes'.DS.'edit'.DS.$recipe->id.'"' ?>>Modifier</a>
<?php endif; ?>
<div class='recipebox'>
<p>
  <?php echo $recipe->description ?>
  <br>
  <?php if ($recipe->picture!=null): ?>
    <img style="max-width: 50%;" src=<?php echo '"'.$recipe->picture.'"' ?> alt="" />
  <?php endif; ?>
  <table>
    <tr>
      <th>Ingrédient</th>
      <th>Quantité</th>
    </tr>
    <?php foreach ($ingredients as $key => $ingredient): ?>
      <tr>
        <td><?php echo $ingredient->ingredient ?></td>
        <td><?php echo $ingredient->quantity ?></td>
      </tr>
    <?php endforeach; ?>
  </table>
  <?php echo $recipe->content ?>
</p>
</div>

<h1>Commentaires :</h1>
  <div class="add_comment_form">
    <form id="add_comment" action=<?php echo '"'.BASE_URL.DS.'recipes'.DS.'view'.DS.$recipe->id.'"' ?> method="post">
      <textarea name="comment" rows="2" cols="40" placeholder="Commentaire"></textarea><br>
      <br>
      <input type="hidden" name="recipes_id" value=<?php echo '"'.$recipe->id.'"' ?>>
      <input type="hidden" name="users_id" value=<?php echo '"'.$this->Session->userId().'"' ?>>
      <button id="button">Ajouter commentaire</button>
    </form>
  </div>

<?php foreach ($comments as $key => $value): ?>
  <div class="comment">
    <div class="user">
      <?php echo $value->pseudo ?>
    </div>
    <div class="message">
      <?php echo $value->comment ?>
    </div>
  </div>
<?php endforeach; ?>
<?php else: ?>
  Veuillez vous connecter
<?php endif; ?>
