<?php if ($this->Session->isLogged()): ?>
<div class="recipe_form">
  <form id="edit_recipe" action=<?php echo '"'.BASE_URL.DS.'recipes'.DS.'edit'.DS.$recipe->id.'"' ?> method="post" enctype="multipart/form-data">
    <input required type="text" name="name" value=<?php echo '"'.$recipe->name.'"' ?> placeholder="Nom de la recette">
    <br>
    <textarea required name="description" rows="4" cols="40" placeholder="Courte descripton de la recette">
      <?php echo $recipe->description ?>
    </textarea><br>
    <div class="ingredients" id="ingredients">
      <div id="ingredients_fields">
        <?php foreach ($ingredients as $key => $ingredient): ?>
          <div class="ingredient">
            <input type="text" name=<?php echo '"ingredient'.intval($key+1).'"' ?> value=<?php echo '"'.$ingredient->ingredient.'"' ?> placeholder="Ingrédient">
            <input type="text" name=<?php echo '"quantity'.intval($key+1).'"' ?> value=<?php echo '"'.$ingredient->quantity.'"' ?> placeholder="Quantité">
          </div>
        <?php endforeach; ?>
      </div>
      <button id="add_ingredient_button" type="button" onclick="addIngredientFields()">Ajouter ingrédient</button>
      <input type="hidden" id="nbIngredients" name="nbIngredients" value=<?php echo '"'.count($ingredients).'"' ?>>
    </div>
    <textarea required name="content" rows="8" cols="40" placeholder="Contenu de la recette">
      <?php echo $recipe->content ?>
    </textarea>
    <br>
    <div style="width:400px;display:inline-block;">
        Recette privée:
        <input type="checkbox" name="private" <?php echo $recipe->private?'checked':'' ?>>
    </div>
    <br>
    <input type="file" name="picture">
    <input type="hidden" name="id"  value=<?php echo '"'.$recipe->id.'"' ?>>
    <input type="hidden" name="users_id" value=<?php echo '"'.$this->Session->userId().'"' ?>>
    <button id="button">Modifier</button>
  </form>
</div>
<?php else: ?>
  Veuillez vous connecter
<?php endif; ?>
