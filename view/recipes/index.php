<?php if ($this->Session->isLogged()): ?>
<div class="btn_add_recipe">
  <a href=<?php echo '"'.BASE_URL.DS.'recipes'.DS.'add'.'"' ?>>Ajouter une recette</a>
</div>
<div class="list_recipes">
  <?php foreach ($recipes as $key => $recipe): ?>
    <div class="recipe">
      <h2>
        <?php echo $recipe->name ?>
      </h2>
      <?php echo $recipe->private?"Privée":"Public" ?>
      <?php echo ' - '.$recipe->pseudo ?>
      <p>
        <?php echo $recipe->description ?>
      </p>
      <a class="action_button" href=<?php echo '"'.BASE_URL.DS.'recipes'.DS.'view'.DS.$recipe->recipes_id.'"' ?>>Accéder à la recette</a>
    </div>
  <?php endforeach; ?>
</div>
<?php else: ?>
  Veuillez vous connecter
<?php endif; ?>
