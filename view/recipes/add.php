<?php if ($this->Session->isLogged()): ?>
<div class="recipe_form">
    <form id="add_recipe" action=<?php echo '"'.BASE_URL.DS. 'recipes'.DS. 'add'. '"' ?> method="post" enctype="multipart/form-data">
        <input required type="text" name="name" value="" placeholder="Nom de la recette">
        <br>
        <textarea required name="description" rows="4" cols="40" placeholder="Courte descripton de la recette"></textarea>
        <br>
        <div class="ingredients" id="ingredients">
            <div id="ingredients_fields">
                <div class="ingredient">
                    <input type="text" name="ingredient1" value="" placeholder="Ingrédient">
                    <input type="text" name="quantity1" value="" placeholder="Quantité">
                </div>
            </div>
            <button id="add_ingredient_button" type="button" onclick="addIngredientFields()">Ajouter ingrédient</button>
            <input type="hidden" id="nbIngredients" name="nbIngredients" value="1">
        </div>
        <textarea required name="content" rows="8" cols="40" placeholder="Contenu de la recette"></textarea>
        <br>
        <div style="width:400px;display:inline-block;">
            Recette privée:
            <input type="checkbox" name="private">
        </div>
        <br>
        <input type="file" name="picture">
        <input type="hidden" name="users_id" value=<?php echo '"'.$this->Session->userId().'"' ?>>
        <button type="submit">Ajouter</button>
    </form>
</div>
<?php else: ?> Veuillez vous connecter
<?php endif; ?>
