<div class="error404">
  <div class="message">
    <h1>Erreur 404</h1>
    <p>La page n'a pas pu être trouvée.</p>
    <p>(<?php echo $message ?>)</p>
  </div>
</div>
<?php header('Location: recipes/index');      

