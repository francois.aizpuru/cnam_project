<?php

/**
 *
 */
class UsersController extends Controller
{
  public function index()
  {
    if($this->Session->isLogged()){
      $this->loadModel('User');
      $users = $this->User->find(array());
      $this->set('users', $users);
    }
  }

  public function view($id=false)
  {
    if($this->Session->isLogged()){
      $this->loadModel('User');
      $user = $this->User->findFirst(array(
        'conditions'  =>  array('id'=>$id)
      ));
      if(empty($user)){
        $this->e404('Utilisateur Introuvable');
      }
      $this->loadModel('Recipe');
      if($this->Session->isAdmin() || ($id && $id==$this->Session->userId())){
        $conditions = array('users_id'=>$id);
      } else {
        $conditions = array('users_id'=>$id,'private'=>false);
      }
      $recipes = $this->Recipe->find(array(
        'conditions'  =>  $conditions
      ));
      $this->set('recipes', $recipes);
    }
  }
}
