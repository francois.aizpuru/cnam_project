<?php

/**
 *
 */
class AdminController extends Controller
{

  function manage()
  {
    if($this->Session->isAdmin()){
      $this->loadModel('User');
      $users = $this->User->find(array(
        'conditions'  =>  array('id!' => $this->Session->userId())
      ));
      $this->set('users', $users);
    } else {
      $location = 'Location: '.BASE_URL.DS.'recipes'.DS.'index';
      header($location);
    }
  }
}
