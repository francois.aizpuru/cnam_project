<?php
/**
 *
 */
class RecipesController extends Controller
{
  function index(){
    if($this->Session->isLogged()){
        $this->loadModel('Recipe');
        $conditions = array(
          'innerjoin' => array(
            'table' => 'users',
            'key' => 'users.id',
            'fkey'  =>  'recipes.users_id'),
            'columns' => 'recipes.id as recipes_id, recipes.description, recipes.name, recipes.private, users.pseudo'
          );
          if(!$this->Session->isAdmin()){
            $cond = 'private=0 OR users_id='.$this->Session->userId();
            $conditions['conditions'] = $cond;
          }
        $recipes = $this->Recipe->find(
          $conditions
        );
        $this->set('recipes', $recipes);
    }
  }


  function view($id=false)
  {
      $this->loadModel('Recipe');
      $this->loadModel('Comment');
      $this->loadModel('Ingredient');

      $recipe = $this->Recipe->findFirst(array(
        'conditions'  => array('id'=>$id)
      ));

      if($recipe->private==0 || $this->Session->isAdmin() || ($recipe->users_id && $recipe->users_id==$this->Session->userId())){
      if($this->request->data){
        $this->Comment->save($this->request->data);
      }


      if(empty($recipe)){
        $this->e404('Recette Introuvable');
      }
      $this->set('recipe', $recipe);
      $comments = $this->Comment->find(array(
        'conditions'  => array('recipes_id'=>$id),
        'innerjoin'       => array(
          'table' => 'users',
          'fkey'   => 'users.id',
          'key'  => 'comments.users_id'
        ),
        'remove'  =>  'password'
      ));
      $ingredients = $this->Ingredient->find(array(
        'conditions'  =>  array('recipes_id'  =>  $id)
      ));
      $this->set('ingredients', $ingredients);
      $this->set('comments', $comments);
    } else {
      $location = 'Location: '.BASE_URL.DS.'recipes'.DS.'index';
      header($location);
    }
  }

  function add(){
    if($this->Session->isLogged()){
      $this->loadModel('Recipe');
      $this->loadModel('Ingredient');

      if($this->request->data){

        $ingredients = array();
        $nbIngredients = $this->request->data->nbIngredients;
        for ($i=1; $i <= $nbIngredients ; $i++) {
          $name = 'ingredient'.$i;
          $quantity = 'quantity'.$i;
          $ingredients[$this->request->data->$name] = $this->request->data->$quantity;
          unset($this->request->data->$name);
          unset($this->request->data->$quantity);
        }
        unset($this->request->data->nbIngredients);
        $this->request->data->private = isset($this->request->data->private) ? 1 : 0;
        $this->Recipe->save($this->request->data);
        foreach ($ingredients as $key => $value) {
          $ingred = new stdClass();
          $ingred->ingredient = $key;
          $ingred->quantity = $value;
          $ingred->recipes_id = $this->Recipe->id;
          $this->Ingredient->save($ingred);
        }
        $filename = 'recipe'.$this->Recipe->id;
        $upload = Utils::upload('picture', $filename, false, array('png','gif','jpg','jpeg'));
        if($upload){
          $recipe = new stdClass();
          $recipe->id = $this->Recipe->id;
          $recipe->picture = $upload;
          $this->Recipe->save($recipe);
        } else {
          $this->set('error', 'Erreur dans le téléchargement du fichier. Vous pouvez le modifier en modifiant la recette.');
        }
      }
    }
  }

  function edit($id=false)
  {
    $this->loadModel('Ingredient');
    $this->loadModel('Recipe');

    $recipe = $this->Recipe->findFirst(array(
      'conditions'  => array('id'=>$id)
    ));

    if($this->Session->isLogged() && $this->Session->isAdmin() || ($recipe->users_id && $recipe->users_id==$this->Session->userId())){
        if($this->request->data){
          $this->Ingredient->delete('recipes_id='.$id);
          $ingredients = array();
          $nbIngredients = $this->request->data->nbIngredients;
          for ($i=1; $i <= $nbIngredients ; $i++) {
            $name = 'ingredient'.$i;
            $quantity = 'quantity'.$i;
            $ingredients[$this->request->data->$name] = $this->request->data->$quantity;
            unset($this->request->data->$name);
            unset($this->request->data->$quantity);
          }
          unset($this->request->data->nbIngredients);
          unset($this->request->data->picture);
          $filename = 'recipe'.$id;
          $upload = Utils::upload('picture', $filename, false, array('png','gif','jpg','jpeg'));
          $this->request->data->picture = $upload ? $upload : null;
          if($this->request->data->picture==null){
            unset($this->request->data->picture);
          }
          $this->request->data->private = isset($this->request->data->private) ? 1 : 0;
          $this->Recipe->save($this->request->data);
          foreach ($ingredients as $key => $value) {
            $ingred = new stdClass();
            $ingred->ingredient = $key;
            $ingred->quantity = $value;
            $ingred->recipes_id = $this->Recipe->id;
            $this->Ingredient->save($ingred);
          }
        }
        $recipe = $this->Recipe->findFirst(array(
          'conditions'  => array('id'=>$id)
        ));
        if(empty($recipe)){
          $this->e404('Recette Introuvable');
        }
        $this->set('recipe', $recipe);
        $ingredients = $this->Ingredient->find(array(
          'conditions'  =>  array('recipes_id'  =>  $id)
        ));
        $this->set('ingredients', $ingredients);

      } else {
        $location = 'Location: '.BASE_URL.DS.'recipes'.DS.'index';
        header($location);
      }
  }
}

 ?>
