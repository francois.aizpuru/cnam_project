<?php

/**
 *
 */
class AuthController extends Controller
{

  public function password($id=false){
    if(!$id || $id==$this->Session->userId() || $this->Session->isAdmin()){
      $this->loadModel('User');
      $this->set('id', $id ? $id : '');
      $data = $this->request->data;
      if($data){
        $data->id = $id ? $id : $this->Session->userId();
        $user = $this->User->find(array(
          'conditions'  =>  array('id'=>$id)
        ));
        if($user){
          $this->User->save($data);
          $location = 'Location: '.BASE_URL.DS.'recipes'.DS.'index';
          header($location);
        }
      }
    } else {
      $location = 'Location: '.BASE_URL.DS.'auth'.DS.'password';
      header($location);
    }
  }

  public function switch($id=false){
    if($this->Session->isAdmin() && $id && $id!=$this->Session->userId()){
      if($id){
        $this->loadModel('User');
        $user = $this->User->findFirst(array(
          'conditions'  =>  array('id'=>$id)
        ));
        if(!empty($user)){
          $user->admin = $user->admin==1 ? 0 : 1;
          $this->User->save($user);
          $location = 'Location: '.BASE_URL.DS.'admin'.DS.'manage';
          header($location);
        }
      }
    }
  }
  
  public function login()
  {
    $data = $this->request->data;
    if($data){
      $this->loadModel('User');
      $user = $this->User->findFirst(array(
        'conditions'  => array(
          'pseudo'=>$data->pseudo,
          'password'=>$data->password
        )
      ));
      if(!empty($user)){
        $this->Session->write('User', $user);
      } else {
        $this->set('error', 'Mauvais couple pseudo/mdp');
      }

    }

    if($this->Session->isLogged()){
      $location = 'Location: '.BASE_URL.DS.'recipes'.DS.'index';
      header($location);
    }
  }

  public function logout()
  {
    if($this->Session->isLogged()){
      $this->Session->write('User',false);
    }
    $location = 'Location: '.BASE_URL.DS.'auth'.DS.'login';
    header($location);

  }

  public function inscription()
  {
    $data = $this->request->data;
    if($data){
      if($data->password===$data->repeat&&!empty($data->password)){
        unset($data->repeat);
        $this->loadModel('User');
        $user = $this->User->findFirst(array(
          'conditions'  => array('pseudo'=>$data->pseudo)
        ));
        if(empty($user)){
          $this->User->save($data);
          $user = $this->User->findFirst(array(
            'conditions'  =>  array('id'=>$this->User->id)
          ));
          $this->Session->write('User',$user);
        } else {
          $this->set('error', "Le pseudo est déjà utilisé");
        }
      } else {
        $this->set('error', "Les deux mots de passe sont différents ou nuls");
      }

    }
    if($this->Session->isLogged()){
      $location = 'Location: '.BASE_URL.DS.'recipes'.DS.'index';
      header($location);
    }
  }
}
