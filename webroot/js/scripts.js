function addIngredientFields() {
  var nbIngredients = document.getElementById("nbIngredients");
  nbIngredients.value = parseInt(nbIngredients.value) + 1;
  var nb = parseInt(nbIngredients.value);
  var ingredients = document.getElementById('ingredients_fields')
  var ingredient = document.createElement("INPUT");
  ingredient.placeholder = "Ingrédient";
  ingredient.name = "ingredient"+nb;
  var quantity = document.createElement("INPUT");
  quantity.placeholder = "Quantité";
  quantity.name = "quantity"+nb;
  var div = document.createElement("DIV");
  div.className = "ingredient";
  div.appendChild(ingredient);
  div.appendChild(quantity);
  ingredients.appendChild(div);
}
