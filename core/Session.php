<?php

/**
 *
 */
class Session
{

  function __construct()
  {
    if(!isset($_SESSION)){
      session_start();
    }
  }

  public function write($key, $value){
    $_SESSION[$key] = $value;
  }

  public function read($key=""){
    if(empty($key)){
      return $_SESSION;
    } else {
      return isset($_SESSION[$key]) ? $_SESSION[$key] : false;
    }
  }

  public function isLogged(){
    return isset($this->read('User')->id);
  }

  public function isAdmin(){
    return isset($this->read('User')->admin) && $this->read('User')->admin==1;
  }

  public function userId(){
    return $this->read('User')->id;
  }
}
