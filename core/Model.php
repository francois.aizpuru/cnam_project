<?php
/**
 *
 */
class Model
{
  static $connections = array();
  public $conf = 'default';
  public $table = false;
  public $db = false;
  public $id;

  public function __construct()
  {
    $conf = Conf::$databases[$this->conf];

    if($this->table === false){
      $this->table = strtolower(get_class($this)).'s';
    }

    if(isset(Model::$connections[$this->conf])){
      $this->db = Model::$connections[$this->conf];
      return true;
    }
    try {
      $pdo = new PDO(
        'mysql:host='.$conf['host'].';dbname='.$conf['database'].';',
        $conf['login'],
        $conf['password'],
        array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
      );
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
      Model::$connections[$this->conf] = $pdo;
      $this->db = $pdo;
    } catch (PDOException $e) {
      if(Conf::debug == 1){
        die($e->getMessage());
      } else {
        die('Impossible de se connecter à la BDD');
      }
    }
  }

  public function delete($req){
    $sql = 'DELETE FROM '.$this->table.' WHERE '.$req;
    $pre = $this->db->prepare($sql);
    $pre->execute();

  }

  public function find($req)
  {
    //INNER JOIN users ON comments.users_id = users.id
    $inner = '';
    if(isset($req['innerjoin'])){
      $inner .= ' INNER JOIN '.$req['innerjoin']['table'].' ON '.$req['innerjoin']['key'].'='.$req['innerjoin']['fkey'];
    }
    if(isset($req['columns'])){
      $sql = 'SELECT '.$req['columns'].' FROM '.$this->table.$inner.' ';
    } else {
      $sql = 'SELECT * FROM '.$this->table.$inner.' ';
    }
    if(isset($req['conditions'])){
      $sql.='WHERE ';
      if(!is_array($req['conditions'])){
        $sql .= $req['conditions'];
      } else {
        $cond = array();
        foreach($req['conditions'] as $k=>$v){
          if(!is_numeric($v)){
            $v = $this->db->quote($v);
          }
          $cond[] = "$k=$v";
        }
        $sql .= implode(' AND ',$cond);
      }
    }
    $pre = $this->db->prepare($sql);
    $pre->execute();
    $toReturn = $pre->fetchAll(PDO::FETCH_OBJ);
    if(isset($req['remove'])){
      foreach ($toReturn as $key => $value) {
        $remove =$req['remove'];
        $value->$remove=null;
      }
    }
    return $toReturn;
  }

  public function findFirst($req)
  {
    return current($this->find($req));
  }

  public function count($req) {
    $sql = 'SELECT COUNT(*) FROM '.$this->table.' WHERE '.$req;
    $pre = $this->db->prepare($sql);
    $pre->execute();
    $result = $pre->fetchAll(PDO::FETCH_OBJ);
  }

  public function save($data){
    $action = '';
    $fields = array();
    $d = array();
    foreach ($data as $key => $value) {
      $fields[] = "$key=:$key";
      $d[":$key"] = $value;
    }
    if(isset($data->id)&&!empty($data->id)){
      //update
      $sql = 'UPDATE '.$this->table.' SET '.implode(',',$fields).' WHERE id=:id';
      $this->id = $data->id;
      $action = 'update';
    } else {
      $sql = 'INSERT INTO '.$this->table.' SET '.implode(',',$fields);
      $action = 'insert';
    }
    $pre = $this->db->prepare($sql);
    $pre->execute($d);
    if($action=='insert'){
      $this->id = $this->db->lastInsertId();
    }
    return true;
  }
}
