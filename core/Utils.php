<?php
/**
 *
 */
class Utils
{
  static function upload($index,$destination,$maxsize=FALSE,$extensions=FALSE)
  {
       if (!isset($_FILES[$index]) OR $_FILES[$index]['error'] > 0) return FALSE;
       if ($maxsize !== FALSE AND $_FILES[$index]['size'] > $maxsize) return FALSE;
       $ext = substr(strrchr($_FILES[$index]['name'],'.'),1);
       if ($extensions !== FALSE AND !in_array($ext,$extensions)) return FALSE;
       $return = BASE_URL.DS.'webroot'.DS.'uploads'.DS.$destination.'.'.$ext;
       $destination = WEBROOT.DS.'uploads'.DS.$destination.'.'.$ext;
       if(move_uploaded_file($_FILES[$index]['tmp_name'],$destination)){
         return $return;
       } else {
         return false;
       }
  }
}
